<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UniversityToSpeciality;

/**
 * UniversityToSpecialitySearch represents the model behind the search form of `app\models\UniversityToSpeciality`.
 */
class UniversityToSpecialitySearch extends UniversityToSpeciality
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'univercity', 'speciality', 'price', 'study_time'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UniversityToSpeciality::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'univercity' => $this->univercity,
            'speciality' => $this->speciality,
            'price' => $this->price,
            'study_time' => $this->study_time,
        ]);

        return $dataProvider;
    }
}
