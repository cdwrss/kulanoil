<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "university_to_speciality".
 *
 * @property int $id
 * @property int $univercity
 * @property int $speciality
 * @property int $price
 * @property int $study_time
 *
 * @property Study[] $studies
 * @property Speciality $speciality0
 * @property University $univercity0
 * @property StudyTime $studyTime
 */
class UniversityToSpeciality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'university_to_speciality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['univercity', 'speciality', 'price', 'study_time'], 'required'],
            [['univercity', 'speciality', 'price', 'study_time'], 'integer'],
            [['speciality'], 'exist', 'skipOnError' => true, 'targetClass' => Speciality::className(), 'targetAttribute' => ['speciality' => 'id']],
            [['univercity'], 'exist', 'skipOnError' => true, 'targetClass' => University::className(), 'targetAttribute' => ['univercity' => 'id']],
            [['study_time'], 'exist', 'skipOnError' => true, 'targetClass' => StudyTime::className(), 'targetAttribute' => ['study_time' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'univercity' => 'Univercity',
            'speciality' => 'Speciality',
            'price' => 'Price',
            'study_time' => 'Study Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudies()
    {
        return $this->hasMany(Study::className(), ['univercity' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeciality0()
    {
        return $this->hasOne(Speciality::className(), ['id' => 'speciality']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnivercity0()
    {
        return $this->hasOne(University::className(), ['id' => 'univercity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudyTime()
    {
        return $this->hasOne(StudyTime::className(), ['id' => 'study_time']);
    }
}
