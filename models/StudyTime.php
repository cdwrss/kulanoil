<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "study_time".
 *
 * @property int $id
 * @property string $name
 *
 * @property UniversityToSpeciality[] $universityToSpecialities
 */
class StudyTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'study_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityToSpecialities()
    {
        return $this->hasMany(UniversityToSpeciality::className(), ['study_time' => 'id']);
    }
}
