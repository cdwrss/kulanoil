<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "speciality".
 *
 * @property int $id
 * @property string $name
 * @property string $short_description
 *
 * @property UniversityToSpeciality[] $universityToSpecialities
 */
class Speciality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'speciality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'short_description'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['short_description'], 'string', 'max' => 64],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'short_description' => 'Short Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityToSpecialities()
    {
        return $this->hasMany(UniversityToSpeciality::className(), ['speciality' => 'id']);
    }
}
