<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "study".
 *
 * @property int $id
 * @property int $univercity
 * @property int $student
 *
 * @property UniversityToSpeciality $univercity0
 * @property Student $student0
 */
class Study extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'study';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['univercity', 'student'], 'required'],
            [['univercity', 'student'], 'integer'],
            [['univercity'], 'exist', 'skipOnError' => true, 'targetClass' => UniversityToSpeciality::className(), 'targetAttribute' => ['univercity' => 'id']],
            [['student'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'univercity' => 'Univercity',
            'student' => 'Student',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnivercity0()
    {
        return $this->hasOne(UniversityToSpeciality::className(), ['id' => 'univercity']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(Student::className(), ['id' => 'student']);
    }
}
