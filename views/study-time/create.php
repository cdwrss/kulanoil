<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StudyTime */

$this->title = 'Create Study Time';
$this->params['breadcrumbs'][] = ['label' => 'Study Times', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="study-time-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
