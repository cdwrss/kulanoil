<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UniversityToSpecialitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'University To Specialities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="university-to-speciality-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create University To Speciality', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=> 'univercity',
                'value'=> 'univercity0.name'
            ],
            [
                'attribute'=> 'speciality',
                'value'=> 'speciality0.name'
            ],
            'price',
            [
                'attribute'=> 'studyTime',
                'value'=> 'studyTime.name'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
