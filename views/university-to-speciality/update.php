<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UniversityToSpeciality */

$this->title = 'Update University To Speciality: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'University To Specialities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="university-to-speciality-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
