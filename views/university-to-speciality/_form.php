<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\University;
use app\models\Speciality;
use app\models\StudyTime;
/* @var $this yii\web\View */
/* @var $model app\models\UniversityToSpeciality */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="university-to-speciality-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'univercity')->dropDownList(
            ArrayHelper::map(University::find()->all(),'id','name'),
            ['prompt'=>'Select University']
       ) ?>

    <?= $form->field($model, 'speciality')->dropDownList(
            ArrayHelper::map(Speciality::find()->all(),'id','name'),
            ['prompt'=>'Select Speciality']
       ) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'study_time')->dropDownList(
            ArrayHelper::map(StudyTime::find()->all(),'id','name'),
            ['prompt'=>'Select Study Time']
       ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
