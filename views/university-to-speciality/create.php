<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UniversityToSpeciality */

$this->title = 'Create University To Speciality';
$this->params['breadcrumbs'][] = ['label' => 'University To Specialities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="university-to-speciality-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
