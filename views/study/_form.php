<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Student;
use app\models\UniversityToSpeciality;
use app\models\University;
use app\models\Speciality;
use app\models\StudyTime;
/* @var $this yii\web\View */
/* @var $model app\models\Study */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="study-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'univercity')->dropDownList(
            ArrayHelper::map(UniversityToSpeciality::find()->asArray()->all(),'id',function($model){
                return University::find()->where(['id' => $model['univercity']])->one()->name."-".Speciality::find()->where(['id' => $model['speciality']])->one()->name."-".StudyTime::find()->where(['id' => $model['study_time']])->one()->name;
            }),
            ['prompt'=>'Select University']
       )?>

    <?= $form->field($model, 'student')->dropDownList(
            ArrayHelper::map(Student::find()->all(),'id','name'),
            ['prompt'=>'Select Student']
       ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
