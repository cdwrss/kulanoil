<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\UniversityToSpeciality;
use app\models\University;
use app\models\Speciality;
use app\models\StudyTime;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StudySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Studies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="study-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Study', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=> 'university',
                'value'=> function ($data)
                    {
                        $univToSpec = UniversityToSpeciality::findOne($data->univercity);
                        $universityId = $univToSpec->univercity;
                        $specialityId = $univToSpec->speciality;
                        $studyType = $univToSpec->studyTime;
                      return University::findOne($universityId)->name.'-'.Speciality::findOne($specialityId)->name.'-'.StudyTime::findOne($studyType)->name;
                    }
            ],

            [
                'attribute'=> 'student',
                'value'=> function($data) {
                    $name = $data->student0->name;
                    $surname = $data->student0->surname;
                    return $name." ".$surname;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
